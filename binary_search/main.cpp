//
//  main.cpp
//  binary_search
//
//  Created by Marta Rusin on 05/11/2022.
//

#include <iostream>
using namespace  std;
int binary_search (int*, int, int);

int main() {
    int p;
//Deklaracja tablicy
    int T[11] = {3, 5, 7, 9, 10, 15, 100, 120, 125, 300, 4000};
// Wczytanie p
    cout << "Podaj szukana liczbe: " <<endl;
    cin >> p;
// Wywolanie funkcji (W3, str 9)
// Wyswietlenie wyniku
    cout << "Wynik: " << binary_search(T, p, 11) <<endl;
    return 0;
}

int binary_search(int* T, int p, int size) {
    int highest  = size;
    int lowest = 0;
    int result;
    
    while(true) {
        if (lowest > highest) {
            result =  -1;
            break;
        }
    
        int middle  = (lowest + highest)/2;
        if (T[middle] <p) {
            lowest = middle + 1;
        }
        if(T[middle] == p) {
            result = middle;
            break;
        }
        if (T[middle] > p) {
            highest = middle - 1;
        }
    };
    return result;

    
};
